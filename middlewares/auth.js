const AuthMiddleware = {};

AuthMiddleware.verifyClient = function(req, res, next) {
    let header_key = process.env.HEADER_KEY;
    console.log('header_key: ', header_key);

    if (req.header(header_key)) {
        let header_value = req.get(header_key);

        if (header_value === process.env.HEADER_VALUE_DEV) {
            console.log('Request is from development');
            return next();
        }
    }

    const error = new Error('Client unauthorized');
    error.status = 401;
    error.code = 401;
    return next(error);
};

module.exports = AuthMiddleware;