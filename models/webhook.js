'use strict';
module.exports = (sequelize, DataTypes) => {
  const Webhook = sequelize.define('Webhook', {
    clientId: DataTypes.STRING,
    reqUrl: DataTypes.STRING,
    reqBody: DataTypes.JSON,
    totalTry: DataTypes.INTEGER,
    totalFailed: DataTypes.INTEGER,
    success: DataTypes.BOOLEAN
  }, {});
  Webhook.associate = function(models) {
    // associations can be defined here
  };
  return Webhook;
};