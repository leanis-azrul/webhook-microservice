// Initialize dependencies
const express = require('express')
const cron = require('node-cron')
const axios = require('axios')

// Load environmental variables
const router = express.Router();
const AuthMiddleware = require('./../middlewares/auth')
const db = require('./../models/index')
const Webhook = db.sequelize.models.Webhook

//Initialize Cron
const task = cron.schedule('* 15 * * * *', () =>  {
    getAllRecordToSchedule()
}, {
    scheduled: false
});

//Testing Purpose - Leanis Header Information
const default_header = {
    'Content-Type': 'application/json',
    'leanis-signature' : 'Dh3F6HaucF9dNuSD6LtoWjE1lcP7u3FN'
}

// const default_header = {
//     'Content-Type': 'application/json'
// }

//Routing
router.post('/createWebhook',AuthMiddleware.verifyClient, (req, res) => {
    Webhook.create({
        clientId: req.body.clientId, 
        reqUrl: req.body.reqUrl, 
        reqBody : req.body.reqBody,
        totalTry:  0,
        totalFailed: 0,
        success: false
    })
    .then(function(result){
        console.log("Data - ", result)
        {
            axios.post(result.reqUrl, result.reqBody,{
                headers : default_header
            })
            .then((res) => {
                console.log("Response From Client", result.data)
                Webhook.update(
                    {totalTry: result.totalTry + 1, success: true},
                    {where: {id: result.id} }
                )
                .then((result) => {
                    console.log("Update Total Try for Id & Success- ", result)
                    return result // promise return to sequlize update
                })
                .catch((err) => {
                    return err // promise return to sequlize update
                })
                return res // promise return to sequlize axios
            })
            .catch((err) => {
                Webhook.update(
                    {totalFailed: result.totalFailed + 1, totalTry: result.totalTry + 1},
                    {where: {id: result.id} }
                )
                .then((result) => {
                    console.log("Update Total Failed for Id - ", result)
                    return result // promise return to sequlize update
                })
                .catch((err) => {
                    return err// promise return to sequlize update
                })
                return err// promise return to sequlize axios
            })
        }
        return res.status(201).json({
            status: 'successful',
            status_code: 201,
            data: result
        });
    })
    .catch(function(err){
        console.error("Error - ", err)
        return res.status(400).json({
            status: 'failed',
            status_code: 400,
            data: err
        })
    })
})

router.get('/startCron', AuthMiddleware.verifyClient, (req, res) => {
    task.start()
    return res.status(201).json({
        status: 'successful',
        status_code: 201,
        data: "Cron Started"
    });
})

router.get('/stopCron', AuthMiddleware.verifyClient, (req, res) => {
    task.stop()
    return res.status(201).json({
        status: 'successful',
        status_code: 201,
        data: "Cron Stopped"
    });
})

router.get('/getAllRecordsForReport',AuthMiddleware.verifyClient, (req, res) => {
    Webhook.findAll({
        raw: true
    })
    .then(function(result){
        console.log("Data - ", result)
        return res.status(201).json({
            status: 'successful',
            status_code: 201,
            data: result
        })
    })
    .catch(function(err){
        console.error("Error - ", err)
        return res.status(400).json({
            status: 'failed',
            status_code: 400,
            data: err
        })
    })
})

router.post('/getRecordForReport',AuthMiddleware.verifyClient, (req, res) => {
    Webhook.findOne({
        where: {clientId: req.body.clientId},
        raw:true
    })
    .then(function(result){
        console.log("Data - ", result)
        return res.status(201).json({
            status: 'successful',
            status_code: 201,
            data: result
        })
    })
    .catch(function(err){
        console.error("Error - ", err)
        return res.status(400).json({
            status: 'failed',
            status_code: 400,
            data: err
        })
    })
})

// Internal Function
function getAllRecordToSchedule(){
    let limit = process.env.RETRY_VALUE;
    Webhook.findAll({
        where: {success: false},
        raw:true
    })
    .then(function(result){
        result.forEach((item) => {
            if (item.totalTry <= limit){
                axios.post(item.reqUrl, item.reqBody,{
                    headers : default_header
                })
                .then((result) => {
                    console.log("Response From Client", result.data)
                    Webhook.update(
                        {totalTry: item.totalTry + 1, success: true},
                        {where: {id: item.id} }
                    )
                    .then((result) => {
                        console.log("Update Total Try for Id & Success- ", result)
                        return result // promise return to sequlize update
                    })
                    .catch((err) => {
                        return err // promise return to sequlize update
                    })
                    return result // promise return to sequlize axios
                })
                .catch((error) => {
                    Webhook.update(
                        {totalFailed: item.totalFailed + 1, totalTry: item.totalTry + 1},
                        {where: {id: item.id} }
                    )
                    .then((result) => {
                        console.log("Update Total Failed for Id - ", result)
                        return result // promise return to sequlize update
                    })
                    .catch((err) => {
                        return err// promise return to sequlize update
                    })
                    return error// promise return to sequlize axios
                })
            }
        })
        return result // promise return to sequlize sequelize query
    })
    .catch(function(err){
        console.error("Error - ", err)
        return err // promise return to sequlize sequelize query
    })
}

module.exports = router;